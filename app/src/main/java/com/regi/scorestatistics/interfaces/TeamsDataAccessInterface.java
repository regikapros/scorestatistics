package com.regi.scorestatistics.interfaces;

import com.regi.scorestatistics.models.Match;
import com.regi.scorestatistics.models.Team;

import java.util.ArrayList;
import java.util.List;

public interface TeamsDataAccessInterface {

    void addTeam(Team team);
    void addTeamList(List<Team> teamList);
    void updateTeamList(int position, Match match);
    ArrayList<Team> getTeamList();
    Team getTeam(String name);
    int getTeamNumber();
    int getMatch(String firstTeamName,String secondTeamName);

}
