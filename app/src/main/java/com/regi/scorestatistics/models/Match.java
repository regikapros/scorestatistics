package com.regi.scorestatistics.models;

public class Match {

    private String teamTwoName;
    private int scoreTeamOne;
    private int scoreTeamTwo;

    public Match(){}

    public Match(String teamTwoName, int scoreTeamOne, int scoreTeamTwo) {
        this.teamTwoName = teamTwoName;
        this.scoreTeamOne = scoreTeamOne;
        this.scoreTeamTwo = scoreTeamTwo;
    }

    public String getTeamTwoName() {
        return teamTwoName;
    }

    public void setTeamTwoName(String teamTwoName) {
        this.teamTwoName = teamTwoName;
    }

    public int getScoreTeamOne() {
        return scoreTeamOne;
    }

    public void setScoreTeamOne(int scoreTeamOne) {
        this.scoreTeamOne = scoreTeamOne;
    }

    public int getScoreTeamTwo() {
        return scoreTeamTwo;
    }

    public void setScoreTeamTwo(int scoreTeamTwo) {
        this.scoreTeamTwo = scoreTeamTwo;
    }



}
