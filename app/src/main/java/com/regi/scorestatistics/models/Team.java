package com.regi.scorestatistics.models;

import java.util.List;

public class Team {

    private String name;
    private List<Match> matches;

    public Team(){}

    public Team(String name, List<Match> matches) {
        this.name = name;
        this.matches = matches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    @Override
    public String toString() {
        return name;
    }
}
