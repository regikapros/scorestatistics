package com.regi.scorestatistics.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.implementations.TeamsDataAccess;
import com.regi.scorestatistics.models.Match;
import com.regi.scorestatistics.models.Team;

import java.util.List;

public class NewMatchDialog extends Dialog implements View.OnClickListener {

    private EditText firstTeamEditText;
    private EditText secondTeamEditText;
    private Spinner firstSpinner;
    private Spinner secondSpinner;

    private TeamsDataAccess teamAccess;
    private Context context;


    public NewMatchDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_score_dialog_template);

        Button teamScoreSaveButton = findViewById(R.id.saveScoreButton);
        firstTeamEditText = findViewById(R.id.firstTeamEditText);
        secondTeamEditText = findViewById(R.id.secondTeamEditText);

        firstSpinner = findViewById(R.id.firstTeamSpinner);
        secondSpinner = findViewById(R.id.secondTeamSpinner);

        firstTeamEditText.setEnabled(true);
        secondTeamEditText.setEnabled(true);

        teamAccess = new TeamsDataAccess();

        teamScoreSaveButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.saveScoreButton){
            String firstScore = firstTeamEditText.getText().toString();
            String secondScore = secondTeamEditText.getText().toString();
            int firstPosition = firstSpinner.getSelectedItemPosition();
            int secondPosition = secondSpinner.getSelectedItemPosition();

            if(!firstScore.equals("") && !secondScore.equals("") && firstPosition!=-1 && secondPosition!=-1){
                int firstScr = Integer.parseInt(firstScore);
                int secondScr = Integer.parseInt(secondScore);
                teamAccess.updateTeamList(firstPosition,new Match(secondSpinner.getSelectedItem().toString(),firstScr,secondScr));
                teamAccess.updateTeamList(secondPosition,new Match(firstSpinner.getSelectedItem().toString(),secondScr,firstScr));
            }

            cancel();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setAdapter();
    }

    private void setAdapter(){
        final List<Team> teamList = teamAccess.getTeamList();
        if(teamList!=null) {
            ArrayAdapter<Team> adapter =
                    new ArrayAdapter<>(context.getApplicationContext(), R.layout.spinner_layout, teamList);
            adapter.setDropDownViewResource(R.layout.spinner_layout);

            firstSpinner.setAdapter(adapter);
            secondSpinner.setAdapter(adapter);
        }
    }


}
