package com.regi.scorestatistics.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.implementations.TeamsDataAccess;
import com.regi.scorestatistics.models.Team;

public class NewTeamDialog extends Dialog implements OnClickListener {

    private EditText teamNameEditText;
    private TeamsDataAccess teamAccess;


    public NewTeamDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_team_dialog_template);
        teamNameEditText = findViewById(R.id.teamNameEditText);
        Button teamNameSaveButton = findViewById(R.id.teamNameSaveButton);

        teamNameEditText.setEnabled(true);
        teamNameSaveButton.setEnabled(true);

        teamAccess = new TeamsDataAccess();

        teamNameSaveButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.teamNameSaveButton){
            String name = teamNameEditText.getText().toString();
            teamAccess.addTeam(new Team(name,null));
            dismiss();
        }
    }
}
