package com.regi.scorestatistics.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.regi.scorestatistics.models.Team;

import java.util.Arrays;
import java.util.List;

public class SharedPref {

    private static SharedPreferences sharedPref;

    public static final String TEAM_NAME = "TEAM_NAME";
    public static final String TEAM_LIST = "TEAM_LIST";
    public static final String STRING_DEFAULT_VALUE = "STRING_DEFAULT_VALUE";

    private SharedPref(){

    }

    public static void init(Context context){
        if(sharedPref == null){
            sharedPref = context.getApplicationContext().getSharedPreferences(context.getPackageName(),Context.MODE_PRIVATE);
        }
    }

    public static String read(String key,String defaultValue){
        return sharedPref.getString(key,defaultValue);
    }

    public static int read(String key,int defaultValue){
        return sharedPref.getInt(key,defaultValue);
    }

    public static void write(String key,String value){
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString(key, value).apply();
    }

    public static void write(String key,Integer value){
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putInt(key, value).apply();
    }

    public static void clear(){
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.clear();
        prefsEditor.apply();
    }

}
