package com.regi.scorestatistics.implementations;

import com.google.gson.Gson;
import com.regi.scorestatistics.interfaces.TeamsDataAccessInterface;
import com.regi.scorestatistics.models.Match;
import com.regi.scorestatistics.models.Team;
import com.regi.scorestatistics.utils.SharedPref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TeamsDataAccess implements TeamsDataAccessInterface {

    private Gson gson;

    public TeamsDataAccess(){
        gson = new Gson();
    }

    @Override
    public void addTeam(Team team) {
        ArrayList<Team> teamList = getTeamList();
        boolean exists = false;
        if(teamList==null) {
            teamList = new ArrayList<>();
        }else{
            for(Team theTeam:teamList){
                if(theTeam.getName().equals(team.getName())){
                    exists = true;
                }
            }
        }
        if(!exists) {
            teamList.add(team);
            String list = gson.toJson(teamList);
            SharedPref.write(SharedPref.TEAM_LIST, list);
        }
    }

    @Override
    public void addTeamList(List<Team> teamList) {
        String list = gson.toJson(teamList);
        SharedPref.write(SharedPref.TEAM_LIST,list);
    }

    @Override
    public void updateTeamList(int position, Match match) {
        ArrayList<Team> teamList = getTeamList();
        Team team = teamList.get(position);
        List<Match> matchList = team.getMatches();
        boolean notFound=true;
        if(matchList==null){
            matchList = new ArrayList<>();
        }
        else{
            for(Match theMatch:matchList){
                if(theMatch.getTeamTwoName().equals(match.getTeamTwoName())){
                    matchList.set(matchList.indexOf(theMatch),match);
                    notFound = false;
                }
            }
        }
        if(notFound){
            matchList.add(match);
        }
        team.setMatches(matchList);
        teamList.set(position,team);

        addTeamList(teamList);
    }

    @Override
    public ArrayList<Team> getTeamList() {
        String teamList = SharedPref.read(SharedPref.TEAM_LIST,SharedPref.STRING_DEFAULT_VALUE);
        if(!teamList.equals(SharedPref.STRING_DEFAULT_VALUE)) {
            Team list[] = gson.fromJson(teamList, Team[].class);
            return new ArrayList<>(Arrays.asList(list));
        }
        return null;
    }

    @Override
    public Team getTeam(String name) {
        ArrayList<Team> teamList = getTeamList();
        for(Team team:teamList){
            if(team.getName().equals(name)){
                return team;
            }
        }
        return null;
    }

    @Override
    public int getTeamNumber() {
        return getTeamList().size();
    }

    @Override
    public int getMatch(String firstTeamName,String secondTeamName) {
        Team team = getTeam(firstTeamName);
        List<Match> matches = team.getMatches();
        if(matches!=null) {
            for (Match match : matches) {
                if (match.getTeamTwoName().equals(secondTeamName)) {
                    return match.getScoreTeamOne();
                }
            }
        }
        return 0;
    }


    public int getPlayedMatches(String name) {
        List<Match> matches = getTeam(name).getMatches();
        if(matches!=null){
            return matches.size();
        }
        return 0;
    }

    public int getWins(String name) {
        List<Match> matches = getTeam(name).getMatches();
        if(matches!=null) {
            int count = 0;
            for (Match match : matches) {
                if (match.getScoreTeamOne() > match.getScoreTeamTwo()) {
                    count++;
                }
            }
            return count;
        }
        return 0;
    }

    public int getScoredGoals(String name) {
        List<Match> matches = getTeam(name).getMatches();
        if(matches!=null) {
            int count = 0;
            for (Match match : matches) {
                count = match.getScoreTeamOne();
            }
            return count;
        }
        return 0;
    }

    public int getConcededGoals(String name) {
        List<Match> matches = getTeam(name).getMatches();
        if(matches!=null) {
            int count = 0;
            for (Match match : matches) {
                count = match.getScoreTeamTwo();
            }
            return count;
        }
        return 0;
    }

    public ArrayList<Team> sortByPlayedMatches() {
        ArrayList<Team> teamList = getTeamList();
        if(teamList!=null) {
            Collections.sort(teamList, new Comparator<Team>() {
                @Override
                public int compare(Team team, Team team2) {
                    return getPlayedMatches(team2.getName()) - getPlayedMatches(team.getName());

                }
            });
        }
        return teamList;
    }

    public ArrayList<Team> sortByWins() {
        ArrayList<Team> teamList = getTeamList();
        if(teamList!=null) {
            Collections.sort(teamList, new Comparator<Team>() {
                @Override
                public int compare(Team team, Team team2) {
                    return getWins(team2.getName()) - getWins(team.getName());

                }
            });
        }
        return teamList;
    }

    public ArrayList<Team> sortByScoredGoals() {
        ArrayList<Team> teamList = getTeamList();
        if(teamList!=null) {
            Collections.sort(teamList, new Comparator<Team>() {
                @Override
                public int compare(Team team, Team team2) {
                    return getScoredGoals(team2.getName()) - getScoredGoals(team.getName());

                }
            });
        }
        return teamList;
    }

    public ArrayList<Team> sortByConcededGoals() {
        ArrayList<Team> teamList = getTeamList();
        if(teamList!=null) {
            Collections.sort(teamList, new Comparator<Team>() {
                @Override
                public int compare(Team team, Team team2) {
                    return getConcededGoals(team2.getName()) - getConcededGoals(team.getName());

                }
            });
        }
        return teamList;
    }


}
