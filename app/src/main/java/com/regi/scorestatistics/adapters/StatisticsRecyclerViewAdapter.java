package com.regi.scorestatistics.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.implementations.TeamsDataAccess;
import com.regi.scorestatistics.models.Team;

import java.util.ArrayList;

public class StatisticsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<Team> teamList;
    private int gridColumn;
    private int size;
    private String header[];
    private TeamsDataAccess teamsDataAccess;
    private String text;

    public StatisticsRecyclerViewAdapter(ArrayList<Team> teamList,String header[],int gridColumn,int size){
        this.teamList = teamList;
        this.gridColumn = gridColumn;
        this.size = size;
        this.header = header;
        teamsDataAccess = new TeamsDataAccess();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item,parent,false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int row = position/gridColumn;
        int column = position-(row*gridColumn);
        if(row==0 && column!=0){
            text = header[column-1];
        }else if(column==0 && row!=0){
            text = teamList.get(row - 1).getName();
        }else if(column==0){
            text = "";
        }else{
            switch (column){
                case 1:{
                    text = String.valueOf(teamsDataAccess.getPlayedMatches(teamList.get(row - 1).getName()));
                    break;
                }
                case 2:{
                    text = String.valueOf(teamsDataAccess.getWins(teamList.get(row - 1).getName()));
                    break;
                }
                case 3:{
                    text = String.valueOf(teamsDataAccess.getScoredGoals(teamList.get(row - 1).getName()));
                    break;
                }
                case 4:{
                    text = String.valueOf(teamsDataAccess.getConcededGoals(teamList.get(row - 1).getName()));
                    break;
                }
            }
        }
        ((ItemViewHolder)holder).setup(text,row,column);

    }

    @Override
    public int getItemCount() {
        return size;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView statisticsText;
        ItemViewHolder(View itemView) {
            super(itemView);
            statisticsText = itemView.findViewById(R.id.itemData);
        }

        void setup(String dataText, int row, int column){
            if(row==0 || column==0) {
                itemView.setBackgroundColor(itemView.getResources().getColor(R.color.coolGray));
            }
                statisticsText.setText(dataText);
        }
    }

    public void sortByPlayedMatches(ArrayList<Team> newList){
        teamList.clear();
        teamList.addAll(newList);
        notifyDataSetChanged();
    }


}
