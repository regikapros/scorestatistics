package com.regi.scorestatistics.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.implementations.TeamsDataAccess;
import com.regi.scorestatistics.models.Team;

import java.util.ArrayList;
import java.util.List;

public class ScoreRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Team> teamList;
    private int size;
    private int gridColumn;
    private TeamsDataAccess teamAccess;

    public ScoreRecyclerViewAdapter(ArrayList<Team> teamList,int size,int gridColumn){
        this.teamList = teamList;
        this.size = size;
        this.gridColumn =gridColumn;
        teamAccess = new TeamsDataAccess();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item,parent,false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int row = position/gridColumn;
        int column = position-(row*gridColumn);
        String text;
        if(column==0 && row!=0){
            text = teamList.get(row-1).getName();
        }else if(row==0 && column!=0){
            text = teamList.get(column-1).getName();
        }else if(column==0){
            text = "";
        }else
            text = String.valueOf(teamAccess.getMatch(teamList.get(row - 1).getName(), teamList.get(column - 1).getName()));
        ((ItemViewHolder)holder).setup(text,row,column);
    }

    @Override
    public int getItemCount() {
        return size;
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder{

        private TextView data;
        ItemViewHolder(View itemView) {
            super(itemView);
            data = itemView.findViewById(R.id.itemData);
        }

        void setup(String dataText, int row, int column){
            if(row==column && row!=0){
                itemView.setBackgroundColor(itemView.getResources().getColor(R.color.colorAccent));
            }else if(row==0 || column==0){
                itemView.setBackgroundColor(itemView.getResources().getColor(R.color.coolGray));
                data.setText(dataText);
            }else
                data.setText(dataText);

        }
    }

}
