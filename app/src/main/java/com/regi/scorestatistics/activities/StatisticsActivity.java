package com.regi.scorestatistics.activities;

import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.adapters.ScoreRecyclerViewAdapter;
import com.regi.scorestatistics.adapters.StatisticsRecyclerViewAdapter;
import com.regi.scorestatistics.implementations.TeamsDataAccess;


public class StatisticsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private final String statisticsHeader[] = {"M","W","GS","GC"};
    private final int STATISTICS_GRID_NUMBER = 5;

    private TeamsDataAccess teamAccess;
    private StatisticsRecyclerViewAdapter statisticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        teamAccess = new TeamsDataAccess();

        if(teamAccess.getTeamList()==null){
            setContentView(R.layout.activity_statistics_no_data);
        }else {

            setContentView(R.layout.activity_statitstics);

            RecyclerView scoreRecyclerView = findViewById(R.id.scoreTable);
            RecyclerView statisticsRecyclerView = findViewById(R.id.statisticsTable);
            Spinner sortSpinner = findViewById(R.id.sortSpinner);

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, statisticsHeader);
            adapter.setDropDownViewResource(R.layout.spinner_layout);

            sortSpinner.setAdapter(adapter);

            sortSpinner.setOnItemSelectedListener(this);

            int columnNum = teamAccess.getTeamNumber() + 1;
            scoreRecyclerView.setLayoutManager(new GridLayoutManager(this, columnNum));
            int size = columnNum * columnNum;
            ScoreRecyclerViewAdapter scoreAdapter = new ScoreRecyclerViewAdapter(teamAccess.getTeamList(), size, columnNum);
            scoreRecyclerView.setAdapter(scoreAdapter);

            statisticsRecyclerView.setLayoutManager(new GridLayoutManager(this, statisticsHeader.length + 1));
            statisticsAdapter = new StatisticsRecyclerViewAdapter(teamAccess.getTeamList(), statisticsHeader, STATISTICS_GRID_NUMBER, STATISTICS_GRID_NUMBER * columnNum);
            statisticsRecyclerView.setAdapter(statisticsAdapter);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch(position){
            case 0:{
                statisticsAdapter.sortByPlayedMatches(teamAccess.sortByPlayedMatches());
                break;
            }
            case 1:{
                statisticsAdapter.sortByPlayedMatches(teamAccess.sortByWins());
                break;
            }
            case 2:{
                statisticsAdapter.sortByPlayedMatches(teamAccess.sortByScoredGoals());
                break;
            }
            case 3:{
                statisticsAdapter.sortByPlayedMatches(teamAccess.sortByConcededGoals());
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
