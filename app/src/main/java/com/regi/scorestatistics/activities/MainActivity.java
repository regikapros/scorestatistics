package com.regi.scorestatistics.activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.regi.scorestatistics.R;
import com.regi.scorestatistics.utils.NewMatchDialog;
import com.regi.scorestatistics.utils.NewTeamDialog;
import com.regi.scorestatistics.utils.SharedPref;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addTeamBtn = findViewById(R.id.addTeamButton);
        Button addScoreBtn = findViewById(R.id.addScoreButton);
        Button showStatisticsBtn = findViewById(R.id.showStatisticsButton);
        Button resetStatisticsBtn = findViewById(R.id.resetStatisticsButton);

        final Dialog newTeamDialog = new NewTeamDialog(this);
        final Dialog newMatchDialog = new NewMatchDialog(this);


        addTeamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newTeamDialog.show();
            }
        });

        addScoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newMatchDialog.show();
            }
        });


        resetStatisticsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPref.clear();
            }
        });

        showStatisticsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
                startActivity(new Intent(MainActivity.this,StatisticsActivity.class));
            }
        });
        SharedPref.init(this);

    }


    private void share(){
        String json  = SharedPref.read(SharedPref.TEAM_LIST,SharedPref.STRING_DEFAULT_VALUE);
        Log.d("TAG", "share: "+json);
    }


}
